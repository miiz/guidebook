//
//  main.m
//  GuideBook
//
//  Created by 高山 良 on 2014/04/26.
//  Copyright (c) 2014年 高山 良. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
