//
//  MainViewController.h
//  GuideBook
//
//  Created by 高山 良 on 2014/04/26.
//  Copyright (c) 2014年 高山 良. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIBarButtonItem* left1 = [[UIBarButtonItem alloc]
                              initWithTitle:@"左1"
                              style:UIBarButtonItemStyleBordered
                              target:self
                              action:nil];
    UIBarButtonItem* left2 = [[UIBarButtonItem alloc]
                              initWithTitle:@"左2"
                              style:UIBarButtonItemStyleBordered
                              target:self
                              action:nil];
    UIBarButtonItem* left3 = [[UIBarButtonItem alloc]
                              initWithTitle:@"左3"
                              style:UIBarButtonItemStyleBordered
                              target:self
                              action:nil];
    self.navigationItem.leftBarButtonItems = @[left1, left2, left3];
    
    UIBarButtonItem* right1 = [[UIBarButtonItem alloc]
                               initWithTitle:@"右1"
                               style:UIBarButtonItemStyleBordered
                               target:self
                               action:nil];
    UIBarButtonItem* right2 = [[UIBarButtonItem alloc]
                               initWithTitle:@"右2"
                               style:UIBarButtonItemStyleBordered
                               target:self
                               action:nil];
    UIBarButtonItem* right3 = [[UIBarButtonItem alloc]
                               initWithTitle:@"右3"
                               style:UIBarButtonItemStyleBordered
                               target:self
                               action:nil];
    self.navigationItem.rightBarButtonItems = @[right1, right2, right3];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
